
#define trigPin  2

#define echoPin0 4
#define echoPin1 5
#define echoPin2 6
#define echoPin3 7

#define led 13

//#include "EnableInterrupt.h"

unsigned long curtime;
unsigned long laststeptime;
bool LEDvar;

// Setup
void setup() {
  Serial.begin (9600);
  pinMode(trigPin,  OUTPUT);
  pinMode(echoPin0, INPUT);
  pinMode(echoPin1, INPUT);
  pinMode(echoPin2, INPUT);
  pinMode(echoPin3, INPUT);
  pinMode(led, OUTPUT);

  laststeptime = millis();

  LEDvar = 0;
}

// Main loop
void loop() {
  int i;
  curtime = millis();
  if ((curtime - laststeptime) >= 200)
  {
    laststeptime += 200;

    // Trigger high pulse for 20 us
    digitalWrite(trigPin, LOW);
    delayMicroseconds(20);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(20);
    digitalWrite(trigPin, LOW);

    // Measurement vars
    char done_count = 0;
    char stat[4] = {0,0,0,0};
    long start[4], stop[4], echo[4];
    long curtime;

    // Grab current time so we can set max
    long measuretime = millis() + 50;

    // Wait for echos on all sensors to return
    while((done_count < 4)) {
      // Get time
      curtime = micros();
      // Read pins
      echo[0] = digitalRead(echoPin0); 
      echo[1] = digitalRead(echoPin1);   
      echo[2] = digitalRead(echoPin2);   
      echo[3] = digitalRead(echoPin3);
      // Process all read pin data
      for (i=0; i<4; i++) {
        switch (stat[i]) {
          // After trigger, wait for low to high transition
          case 0:
            if (echo[i] == HIGH) {
              stat[i]++;          // Indicate at next step
              start[i] = curtime; // Grab start time
            }
            break;
          // Echo signal high, wait for high to low transition
          case 1:
            if (echo[i] == LOW) {
              stat[i]++;         // Indicate done
              stop[i] = curtime; // Grab stop time
              done_count++;      // Increment done time
            }
            break;
          default:
            break;
        }
      }
      // Wait 50ms max for measurement to return
      if (millis() > measuretime) break;
    }

    // Calculate distance and send to pi
    int i;
    long duration[4];
    long distance[4];
    unsigned char writebytes[8];
    unsigned char offset;
    for (i=0; i<4; i++)
    {
      duration[i] = stop[i]-start[i];
      // Check to make sure if sensor actually measured anything.
      // If not, measurement is bad
      if (stat[i] != 2)
      {
        distance[i] = 1023;
        //Serial.print(" BAD, ");
      }
      // Otherwise, calculate duration
      else 
      {
        distance[i] = (duration[i] / 2) / 29.1;
        if (distance[i] > 1023) distance[i] = 1023;
        //Serial.print(distance[i]);
        //Serial.print(" cm, ");
      }
      offset = i*2;
      writebytes[offset]   = (( offset    & 0x7) << 5) |  (distance[i] & 0x001F);
      writebytes[offset+1] = (((offset+1) & 0x7) << 5) | ((distance[i] & 0x03E0) >> 5);
    }
    // Write all bytes
    Serial.write(writebytes,8);
    //Serial.println(" ");

    // Heartbeat LED
    LEDvar = !LEDvar;  
    digitalWrite(led, LEDvar);
  }

}



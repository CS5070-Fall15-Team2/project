#!/bin/bash

# Remember Current Directory
CURDIR=`pwd`

# Set project root directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE_ROOT=$(echo $DIR | sed -e 's/\(bin\)*$//g')

if [ "$1" = "clean" ]; then
  rm -rf $BASE_ROOT/sonar/.build
fi

cd $BASE_ROOT/sonar

ino build

cd $CURDIR

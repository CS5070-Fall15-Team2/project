#!/bin/bash

# Remember Current Directory
CURDIR=`pwd`

# Set project root directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE_ROOT=$(echo $DIR | sed -e 's/\(bin\)*$//g')

cd $BASE_ROOT/sonar

$BASE_ROOT/bin/buildSonar.sh

TTYUSBVal=
if [ ! -z $1 ]; then
  ino upload -p $1
else
  ino upload
fi

cd $CURDIR

#!/bin/bash

# Remember Current Directory
CURDIR=`pwd`

# Set project root directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE_ROOT=$(echo $DIR | sed -e 's/\(bin\)*$//g')

source /opt/ros/indigo/setup.bash

CATKINBUILD=$BASE_ROOT/catkin_ws

if [ "$1" = "cleanonly" ]; then
  rm -rf $CATKINBUILD/
  exit 0
fi

if [ "$1" = "clean" ]; then
  rm -rf $CATKINBUILD/
fi

if [ ! -d $CATKINBUILD ]; then
  mkdir -p $CATKINBUILD/src
fi

rsync -a $BASE_ROOT/turtle_sonar $CATKINBUILD/src
#rsync -a $BASE_ROOT/turtle_navigation $CATKINBUILD/src
#rsync -a $BASE_ROOT/turtle_hardware_interface $CATKINBUILD/src
#rsync -a $BASE_ROOT/turtle_vision $CATKINBUILD/src

cd $CATKINBUILD && catkin_make

cd $CURDIR

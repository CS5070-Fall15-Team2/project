#!/bin/bash

# Remember Current Directory
CURDIR=`pwd`

# Set project root directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE_ROOT=$(echo $DIR | sed -e 's/\(bin\)*$//g')

#source /opt/ros/indigo/setup.bash
source $BASE_ROOT/catkin_ws/devel/setup.bash

cd $CURDIR

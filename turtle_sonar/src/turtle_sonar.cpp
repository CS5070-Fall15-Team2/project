#include "turtle_sonar.h"
#include <cstdio>

#define SAMPLES_PER_SEC 5

// Defines for different sonar sensors
#define FRONT 0
#define RIGHT 1
#define BACK  2
#define LEFT  3

namespace turtle_sonar
{
    
    TurtleSonar::TurtleSonar( const char *tty_path )
    {
        // open serial port

        serial_port_.setPort( tty_path );
        serial_port_.setBaudrate( 9600 );
        serial::Timeout timeout( serial::Timeout::simpleTimeout(1000) );
        serial_port_.setTimeout( timeout );
        try
        {
            serial_port_.open();
        }
        catch ( std::exception &e )
        {
            ROS_ERROR_STREAM( "could not open " << tty_path << " for read/write" );
        }

        // flush serial port to rid of old data
        serial_port_.flushInput();
        
        // initialize state variables
/*
        pos_[LEFT] = pos_[RIGHT] = 0;
        vel_[LEFT] = vel_[RIGHT] = 0;
        eff_[LEFT] = eff_[RIGHT] = 0;
        cmd_[LEFT] = cmd_[RIGHT] = 0;
*/
        distance[FRONT] = distance[RIGHT] = distance[BACK] = distance[LEFT] = 0.0;
/*      
        // connect and register the joint state interface
        hardware_interface::JointStateHandle state_handle_a("left_motor", &pos_[LEFT], &vel_[LEFT], &eff_[LEFT]);
        jnt_state_interface_.registerHandle(state_handle_a);
        
        hardware_interface::JointStateHandle state_handle_b("right_motor", &pos_[RIGHT], &vel_[RIGHT], &eff_[RIGHT]);
        jnt_state_interface_.registerHandle(state_handle_b);
        
        registerInterface(&jnt_state_interface_);
        
        // connect and register the joint velocity interface
        hardware_interface::JointHandle vel_handle_a(jnt_state_interface_.getHandle("left_motor"), &cmd_[LEFT]);
        jnt_vel_interface_.registerHandle(vel_handle_a);
        
        hardware_interface::JointHandle vel_handle_b(jnt_state_interface_.getHandle("right_motor"), &cmd_[RIGHT]);
        jnt_vel_interface_.registerHandle(vel_handle_b);
        
        registerInterface(&jnt_vel_interface_);
*/        
        // create the controller manager
        ROS_DEBUG_STREAM_NAMED("hardware_interface","Loading controller_manager");
        controller_manager_.reset(new controller_manager::ControllerManager(this, nh_));
/*        
        // subscribe
        sub_ = nh_.subscribe("cmd_vel",                                // topic
                              20,                                      // queue_size
                              &TurtleHardwareInterface::keymsg, this); // function to call
*/
        // start the update loop
        ros::Duration update_freq = ros::Duration(1.0/SAMPLES_PER_SEC);
        update_loop_ = nh_.createTimer(SAMPLES_PER_SEC, &TurtleSonar::update, this);
    }
    
    TurtleSonar::~TurtleSonar()
    {
        // close serial port
        serial_port_.close();
    }
    
    void TurtleSonar::read()
    {
        uint8_t frame_bytes[8];
        uint16_t sensor_readings[4];
        size_t  bytes_read;

        // if serial port is not open, do nothing
        if (serial_port_.isOpen())
        {
          // If too many bytes flush them all
          if (serial_port_.available() > 8) {
              ROS_ERROR_STREAM("Too many bytes available: " << serial_port_.available() << ". Flushing...");
              serial_port_.flushInput();
          }
          // Check for frame from arduino
          else if (serial_port_.available() > 0)
          {
            // Keep reading until a single byte is read
            while (serial_port_.read(frame_bytes, 1) != 1);

            unsigned char seqnum = (frame_bytes[0] >> 5) & 0x07;

            // Make sure sequence number is 0. If not, flush bytes depending on
            // sequence number read
            if (seqnum > 0)
            {
              int count = 0;
              int numbytes = 7 - seqnum;
              while (count < 7 - seqnum) {
                bytes_read = serial_port_.read(&frame_bytes[1+count], numbytes);
                count += bytes_read;
                numbytes = numbytes - bytes_read;
              }
              ROS_INFO_STREAM("Incomplete frame recieved (seqnum " << seqnum << "). Flushing remaining bytes...");
            }
            // If correct, collect remaining 7 bytes and process
            else
            {
              int count = 0;
              int numbytes = 7;
              while (count < 7) {
                bytes_read = serial_port_.read(&frame_bytes[1+count], numbytes);
                count += bytes_read;
                numbytes = numbytes - bytes_read;
              }

              // incoming packet format from Arduino
              //  7   5 4                     0
              // +-----+-----------------------+
              // | aa0 |        UP[4:0]        | BYTE A+0
              // +-----+-----------------------+
              //  7   5 4                     0
              // +-----+-----------------------+
              // | aa1 |        LW[4:0]        | BYTE A+1
              // +-----+-----------------------+
              //
              // 'A' corresponds to a rangefinder (0-3)
              //  - Range finder #0 is first, #1 is next, etc...
              // 'aa' is rangefinder ID in binary
              // 'UP' is upper 5 bits of range in cm
              // 'LW' is lower 5 bits of range in cm
              // '((UP << 5) | LW)' is unsigned 10-bit integer of range in cm
              
              // Go through each range finder fields and extract range
              for (int i=0; i<4; i++) {
                // Grab upper 5 bits
                sensor_readings[i]  = frame_bytes[(2*i)+1];
                sensor_readings[i]  = (sensor_readings[i] << 5) & 0x03E0;
                // Grab lower 5 bits
                sensor_readings[i] |= frame_bytes[(2*i)+0] & 0x001F;
              }

              // Print debug information
              char tempstr[32];
              //sprintf( tempstr, "%X,%X,%X,%X,%X,%X,%X,%X.", frame_bytes[0], frame_bytes[1], frame_bytes[2], frame_bytes[3], frame_bytes[4], frame_bytes[5], frame_bytes[6], frame_bytes[7] );
              //ROS_ERROR_STREAM("Serial bytes recieved: " << tempstr);
              sprintf( tempstr, "%i cm, %i cm, %i cm, %i cm.", sensor_readings[0], sensor_readings[1], sensor_readings[2], sensor_readings[3]);
              ROS_ERROR_STREAM("Sensor readings recieved: " << tempstr);
            }
          }
        }
    }
    
    void TurtleSonar::update(const ros::TimerEvent& e)
    {
        // calculate time elapsed since last update
        elapsed_time_ = ros::Duration(e.current_real - e.last_real);
        
        // read from motor controller
        read();
        
        // perform calculations here (kalman filter, etc..)
        // kalman_filter(); // example function call

        // update controller manager
        //controller_manager_->update(ros::Time::now(), elapsed_time_);
        
    }

}

int main( int argc, char **argv )
{
    // initialize node
    ros::init(argc, argv, "turtle_sonar");
    
    // allow the action server to receive and send ros messages
    ros::AsyncSpinner spinner(4);
    spinner.start();
    
    // create the ROS node handle
    ros::NodeHandle nh;

    // get tty path from arguments
    std::string serial_name( "/dev/ttyACM0" );
    
    for ( int i = 0; i < argc; i++ )
    {
        if ( std::string(argv[i]).compare("--serial") == 0 )
        {
            if ( argc > i+1 )
            {
                serial_name = argv[i+1];
            }
        }
    }
    
    // instantiate hardware interface
    turtle_sonar::TurtleSonar robot(serial_name.c_str());
    
    // spin until ROS tells us to stop
    ros::spin();
    
    return 0;
}


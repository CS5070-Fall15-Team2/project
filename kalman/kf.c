/*****************************************************************
   Title: Kalman filter demo
   Notes: Moved from ROS turtle software.
   Date: 15 Dec 15
   Authors: CS 5070 Team 2
   Description: This code simulates a robot equipped with an ultrasonic 
                rangefinder approaching a barrier. The resulting Kalman filter 
                uses the motion of the robot and the measurements from the 
                sensor to estimate the robot state.
                Attempts to determine theta and y of the robot pose from the 
                single measurement proved unsuccessful, perhaps because the error
                in wheel odometry correlate to the sensor/barrier range.

                The values used for this simulation show good convergence, even with a 
                poor initial estimate of the starting position and sensor covariance. 

******************************************************************/
#include <stdio.h>
#include <math.h>
#include <time.h>

#define MAX_TIME 90
#define START_POS 1.00  // Position at tick = 0
                                                                                                                                                                                                                                                                                       #define VELOCITY 0.01  // Velocity in m/sec

float actual_x = START_POS; 
float actual_y = 0;
float actual_theta = 0;

float est_x, est_y, est_theta;
float prior_x, prior_y, prior_theta;

float cmd_x;
float cmd_y = 0;
float cmd_theta = 0;

float ultra_cov = 0.02;  	// Standard deviation of the ultrasonic sensor 
				// Note that all these values are greater than
				// measured to show filter convergence.
float rt_odom_stddev = 0.05; 	// Standard deviation for wheel odometry (right)
float lt_odom_stddev = 0.05; 	// and for the left odometer
float wheelbase = 0.15; 	// robot wheelbase is 15 cm

float prior_err_cov = 1; 	// Initialize to one because it will converge.
float kalman_gain; 
float ultra_meas;


int tick; 	// simulated time in seconds



int main () {

  
  srand(time(NULL));

  est_x = START_POS + 0.20; // Purposely make the initial estimate poor.
  est_y = 0.0;
  est_theta = 0.0;
  kalman_gain = (1.0/(1+ultra_cov));

  for (tick=1; tick<MAX_TIME; tick++){

    cmd_x = VELOCITY;

    // Move the robot.  

    // Simulate the errors in the wheel odometry.

    float random = 1.0 - ((float)(rand() % 2000)/1000.0);
    float rt_odom_step = cmd_x + cmd_x * random * rt_odom_stddev; 
    random = 1.0 - ((float)(rand() % 2000)/1000.0);
    float lt_odom_step = cmd_x + cmd_x * random * rt_odom_stddev;

    
    // printf("right %f\tleft %f\n", rt_odom_step, lt_odom_step);    

    // With the left and right wheel movements, calculate the actual state for the simulation.
    float theta_step = (lt_odom_step - rt_odom_step)/wheelbase;
    float move_step = (lt_odom_step+rt_odom_step)/2;
    actual_theta += theta_step;

    // printf("right %f\tleft %f theta: %f\n", rt_odom_step, lt_odom_step, theta_step);    

    float x_step = cos(actual_theta)*move_step;
    float y_step = sin(actual_theta)*move_step;

    actual_x -= x_step;
    actual_y += y_step; 
    // printf ("%d\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t\n", tick, est_x, est_y, est_theta, actual_x, actual_y, actual_theta);	
 
    // Predict the state based on the command and the prior state.  
    // Note that since no y or theta is commanded, neither is update here.
 
    //    est_x -=  cmd_x*cos(est_theta);
    //    est_y +=  cmd_x*sin(est_theta);
    //    est_theta = est_theta; // For completeness this will be updated in the next step

    // Measurement update
    random = 1.0 - ((float)(rand() % 2000)/1000.0);
    ultra_meas = actual_x + random*ultra_cov; 
    // printf("random: %1.3f  %1.3f  actual: %1.3f\n", random, random*ultra_cov, actual_x);
    
    kalman_gain = prior_err_cov/(prior_err_cov + ultra_cov);
    // printf("prior x: %1.3f  kalman gain: %1.3f  measurement: %1.3f\t", est_x, kalman_gain, ultra_meas);
    est_x = est_x + kalman_gain*(ultra_meas-est_x); 
    // printf("new x: %1.3f\n", est_x);
    prior_err_cov = (1.0 - kalman_gain) * prior_err_cov;

    printf("State estimate: (%1.3f,%1.3f,%1.3f)  Kaman filter covariance: %1.3f\n", 
        est_x, est_y, est_theta, prior_err_cov); 
     
  }

}

